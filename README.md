# Bootcamp
_________
# *Sagar Timalsena*

I'm Sagar from Nepal, and I do content on Design and Development. I really enjoy learning programming languages . I also enjoy  ui, ux, and design in general. 


____
## Skills and Experience
* ⚛ Bootstrap
*  Flutter
* 💻 HTML, CSS, JS

____

## Examples of Work 
Flutter Project Output:
http://www.timalsenasagar.com.np/saagar/Images/Profile.png

________
Web Development:

[PersonalWebsite](http://www.timalsenasagar.com.np)

![Photo](https://qph.fs.quoracdn.net/main-thumb-453890178-200-gewcwbqbkgvqatbztrbxogjymtystrkm.jpeg)

orderlist
1. Order1
2. Order2
3. Order3
4. Order4
5. Order5
   
---  
Unorder List
- Unorderlist 1
- Unorderlist 2
- Unorderlist 3
- Unorderlist 4
- Unorderlist 5

```


            while (! ( succeed = try() ) ) ;

``` 
___

|S.N|Name       | Address      | Mail Address    |
|----|-------|--------------|-----------------|
|1|Sagar Timalsena|USA|sagar@acd.edu.np
|2|Mr. XYZ| India| xyz@mail.com

___
## To DO List

- [ ] Task 1
- [x] Task 2
- [x] Task 3
- [ ] Task 4

___
## Badges









